package com.koala.common.sentinel;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.koala.common.api.RestfulRequest;
import com.koala.common.api.RestfulResponse;
import com.koala.common.api.bean.ResponseHeader;

public class SentinelDefaultBlockHandler {

    public static RestfulResponse defaultBlockHandler(RestfulRequest request, BlockException e) {

        ResponseHeader responseHeader = new ResponseHeader();
        setStateFusing(responseHeader, e);

        RestfulResponse response = new RestfulResponse();
        response.setResponseHeader(responseHeader);

        return response;
    }

    private static void setStateFusing(ResponseHeader responseHeader, BlockException e) {
        responseHeader.setStateFusing("501", e.getRule().getResource());
    }

}
