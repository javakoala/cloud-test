package com.koala.common.sentinel;

import com.koala.common.api.RestfulResponse;
import com.koala.common.api.bean.ResponseHeader;

public class SentinelDefaultFallback {

    /**
     * 在Sentinel1.7中全局fallback方法不能包含除Throwable之外的参数
     * @param e
     * @return
     */
    public static RestfulResponse defaultFallback(Throwable e) {

        ResponseHeader responseHeader = new ResponseHeader();
        setFailure(responseHeader, e);

        RestfulResponse response = new RestfulResponse();
        response.setResponseHeader(responseHeader);

        return response;
    }

    private static void setFailure(ResponseHeader responseHeader, Throwable e) {
        responseHeader.setStateFailure("500", e.getMessage());
    }

}
