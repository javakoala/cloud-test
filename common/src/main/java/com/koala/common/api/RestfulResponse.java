package com.koala.common.api;

import com.koala.common.api.base.BaseResponse;
import com.koala.common.api.bean.ResponseHeader;

import java.io.Serializable;

public class RestfulResponse<T extends BaseResponse> implements Serializable {

    private ResponseHeader responseHeader;

    public RestfulResponse() {
        this.responseHeader = new ResponseHeader();
    }

    T payload;

    public ResponseHeader getResponseHeader() {
        return responseHeader;
    }

    public void setResponseHeader(ResponseHeader responseHeader) {
        this.responseHeader = responseHeader;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public void check() {
        responseHeader.check();
    }

    @Override
    public String toString() {
        return "RestfulResponse{" +
                "responseHeader=" + responseHeader +
                ", payload=" + payload +
                '}';
    }
}
