package com.koala.common.api;

import com.koala.common.api.base.BaseRequest;
import com.koala.common.api.bean.RequestHeader;

import java.io.Serializable;

public class RestfulRequest<T extends BaseRequest> implements Serializable {

    RequestHeader requestHeader;

    T payload;

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    public void setRequestHeader(RequestHeader requestHeader) {
        this.requestHeader = requestHeader;
    }

    @Override
    public String toString() {
        return "RestfulRequest{" +
                "requestHeader=" + requestHeader +
                ", payload=" + payload +
                '}';
    }
}
