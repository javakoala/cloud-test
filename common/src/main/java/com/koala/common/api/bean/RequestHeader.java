package com.koala.common.api.bean;

import java.io.Serializable;

public class RequestHeader implements Serializable {

    private Long timestamp;

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
