package com.koala.common.api.bean;

import java.io.Serializable;

public class ResponseHeader implements Serializable {

    private static final int STATE_FAILURE = 0;
    private static final int STATE_SUCCESS = 1;
    private static final int STATE_FUSING = 2;

    private int state = STATE_SUCCESS;

    private String errorCode;
    private String errorMessage;

    private Long timestamp = System.currentTimeMillis();

    public int getState() {
        return state;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setStateFailure(String errorCode, String errorMessage) {
        this.state = STATE_FAILURE;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public void setStateFusing(String errorCode, String errorMessage) {
        this.state = STATE_FUSING;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public void check() {
        if(state != STATE_SUCCESS) {
            throw new RuntimeException(errorMessage);
        }
    }

    @Override
    public String toString() {
        return "ResponseHeader{" +
                "state=" + state +
                ", errorCode='" + errorCode + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
