## 介绍

### 项目简介

本项目主要通过一个简单的业务模拟微服务下多服务，多数据库的应用场景，主要目的在于测试一些微服务组件。项目中只进行基本的功能测试，不涉及某些服务的持久化与集群部署。

### 测试目标及对应组件

* 注册中心（Nacos）
* 配置中心（Nacos）
* 接口限流（Sentinel）
* 远程调用（RestTemplate）
* 进程级负载均衡（Ribbon）
* 分布式事务
  * Seata-AT
  * RocketMQ

### 微服务划分

* payment-service（支付服务）
* order-service（订单服务）
* repertory-service（仓库服务）

### 业务描述

1. 用户调用“支付服务”进行支付
2. “支付服务”将用户金额存库
3. “支付服务”通过MQ通知订单服务（最终一致性）
4. “订单服务”创建订单并存库
5. “订单服务”调用“仓库服务”创建出库单（强一致性）

### 项目依赖环境

* JDK8
* Maven 3.0
* Docker
* Nacos 1.3.0
* Sentinel 1.7.2
* Seata 1.0.0
* MySql 5.7
* RocketMQ - NameServer 4.5.0
* RocketMQ - Broker 4.5.0
* RocketMQ - Console latest

### 服务地址

* Nacos
  * http://localhost:8848/nacos
  * 账号：nacos
  * 密码：nacos
* Sentinel
  * http://localhost:8080/
  * 账号：sentinel
  * 密码：sentinel
* RocketMQ-Console
  * http://localhost:8180/
* payment-service
  * http://localhost:5000
* order-service
  * http://localhost:5001
* repertory-service
  * http://localhost:5002

## 依赖服务部署

### 一、下载并导入镜像

百度网盘：

链接:https://pan.baidu.com/s/1RCSdhHSFVVQ8yqaKGrRvfA  密码:zvn5

### 二、部署MySql

参考：https://hub.docker.com/_/mysql

### 三、部署Nacos

```yaml
docker run \
--name nacos-1.3.0 \
-p 8848:8848 \
-e MYSQL_SERVICE_HOST="127.0.0.1" \
-e MYSQL_SERVICE_PORT="3306" \
-e MYSQL_SERVICE_DB_NAME="nacos" \
-e MYSQL_SERVICE_USER="root" \
-e MYSQL_SERVICE_PASSWORD="root" \
-e MODE="standalone" \
-d nacos/nacos-server:1.3.0
```

### 四、部署Sentinel

```yaml
docker run \
--name sentinel \
-p 8080:8080 \
-d koala/sentinel-dashboard:1.7.2
```

### 五、部署Seata-Server

1. 将项目中"resources/conf/seata-server/"中的配置文件拷贝到你的硬盘。
2. 修改配置文件中的ip地址，改为你本机的ip。
   * file.conf
     * 修改数据库连接信息
       * url = "jdbc:mysql://192.168.18.9:3306/seata" # 修改为你的本机ip
       * user = "root"          # 修改为你本机的mysql账号
       * password = "root" # 修改为你本机的mysql密码
   * registry.conf
     * 修改nacos配置
       * serverAddr = "192.168.18.9:8848" # 修改为你的本机ip
3. 启动Seata服务

```yaml
docker run --name seata-server \
-p 8091:8091 \
-e SEATA_CONFIG_NAME=file:/root/seata-config/registry \
-v ${your_local_path}/conf:/root/seata-config  \
-d seataio/seata-server:1.0.0
```

### 六、部署RocketMQ

#### 1、部署NameServer

```yaml
docker run -d \
--name rmqnamesrv \
-v ${your_local_path}/mqnamesrv/logs:/opt/logs  \
-v ${your_local_path}/mqnamesrv/store:/opt/store  \
-e "JAVA_OPT_EXT= -Duser.home=/opt -Xms512M -Xmx512M -Xmn128m" \
-p 9876:9876 \
rocketmqinc/rocketmq-namesrv:4.5.0-alpine \
sh mqnamesrv
```

#### 2、部署Broker

```yaml
docker run -d   \
--name rmqbroker \
-e "JAVA_OPT_EXT=-Xms512M -Xmx512M -Xmn128m" \
-p 10911:10911 \
-p 10909:10909 \
-v ${your_local_path}/broker/logs:/opt/logs  \
-v ${your_local_path}/broker/store:/opt/store  \
-v ${your_local_path}/conf/broker.conf:/etc/rocketmq/broker.conf \
rocketmqinc/rocketmq-broker:4.5.0-alpine \
sh mqbroker -c /etc/rocketmq/broker.conf -n 192.168.18.9:9876:9876 autoCreateTopicEnable=true
```

#### 3、Broker配置文件（broker.conf）

注意修改brokerIP1=192.168.18.9为你的本机ip

```pro
#所属集群名字
brokerClusterName=DefaultCluster
 
#broker名字，注意此处不同的配置文件填写的不一样，如果在broker-a.properties使用:broker-a,
#在broker-b.properties使用:broker-b
brokerName=broker-a
 
#0 表示Master，>0 表示Slave
brokerId=0
 
#nameServer地址，分号分割
#namesrvAddr=rocketmq-nameserver1:9876;rocketmq-nameserver2:9876
 
#启动IP,如果 docker 报 com.alibaba.rocketmq.remoting.exception.RemotingConnectException: connect to <192.168.18.9:10909> failed
# 解决方式1 加上一句producer.setVipChannelEnabled(false);，解决方式2 brokerIP1 设置宿主机IP，不要使用docker 内部IP
brokerIP1=192.168.18.9
 
#在发送消息时，自动创建服务器不存在的topic，默认创建的队列数
defaultTopicQueueNums=4
 
#是否允许 Broker 自动创建Topic，建议线下开启，线上关闭
autoCreateTopicEnable=true
 
#是否允许 Broker 自动创建订阅组，建议线下开启，线上关闭
autoCreateSubscriptionGroup=true
 
#Broker 对外服务的监听端口
listenPort=10911
 
#删除文件时间点，默认凌晨4点
deleteWhen=04
 
#文件保留时间，默认48小时
fileReservedTime=120
 
#commitLog每个文件的大小默认1G
mapedFileSizeCommitLog=1073741824
 
#ConsumeQueue每个文件默认存30W条，根据业务情况调整
mapedFileSizeConsumeQueue=300000
 
#destroyMapedFileIntervalForcibly=120000
#redeleteHangedFileInterval=120000
#检测物理文件磁盘空间
diskMaxUsedSpaceRatio=88
#存储路径
#storePathRootDir=/home/ztztdata/rocketmq-all-4.1.0-incubating/store
#commitLog 存储路径
#storePathCommitLog=/home/ztztdata/rocketmq-all-4.1.0-incubating/store/commitlog
#消费队列存储
#storePathConsumeQueue=/home/ztztdata/rocketmq-all-4.1.0-incubating/store/consumequeue
#消息索引存储路径
#storePathIndex=/home/ztztdata/rocketmq-all-4.1.0-incubating/store/index
#checkpoint 文件存储路径
#storeCheckpoint=/home/ztztdata/rocketmq-all-4.1.0-incubating/store/checkpoint
#abort 文件存储路径
#abortFile=/home/ztztdata/rocketmq-all-4.1.0-incubating/store/abort
#限制的消息大小
maxMessageSize=65536
 
#flushCommitLogLeastPages=4
#flushConsumeQueueLeastPages=2
#flushCommitLogThoroughInterval=10000
#flushConsumeQueueThoroughInterval=60000
 
#Broker 的角色
#- ASYNC_MASTER 异步复制Master
#- SYNC_MASTER 同步双写Master
#- SLAVE
brokerRole=ASYNC_MASTER
 
#刷盘方式
#- ASYNC_FLUSH 异步刷盘
#- SYNC_FLUSH 同步刷盘
flushDiskType=ASYNC_FLUSH
 
#发消息线程池数量
#sendMessageThreadPoolNums=128
#拉消息线程池数量
#pullMessageThreadPoolNums=128
```

#### 4、部署Rocket-Console

注意修改为你本机ip

```yaml
docker run -d \
--name rmqconsole \
-e "JAVA_OPTS=-Drocketmq.namesrv.addr=192.168.18.9:9876 -Dcom.rocketmq.sendMessageWithVIPChannel=false" \
-p 8180:8080 \
styletang/rocketmq-console-ng \
--networks rocketmq
```

## 测试

### 项目启动

1. 将项目clone到本地
2. 使用IDEA等开发工具导入项目
3. 执行sql脚本初始化数据库project/resources/script/ddl/*
4. 启动项目
   1. payment-service
   2. order-service
   3. repertory-service

### 测试

地址：http://localhost:5000

请求：POST

信令：

```json
{
  "requestHeader": {
    "timestamp":"1594458608"
  },
  "payload": {
  	"cost":5000
  }	
}
```





