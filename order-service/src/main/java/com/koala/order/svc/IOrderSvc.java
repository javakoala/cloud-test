package com.koala.order.svc;

import com.koala.order.entity.OrderEntity;

public interface IOrderSvc {

    void save(OrderEntity orderEntity);

    OrderEntity load(String id);

}
