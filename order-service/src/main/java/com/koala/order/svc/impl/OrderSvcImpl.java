package com.koala.order.svc.impl;

import com.koala.common.api.RestfulRequest;
import com.koala.common.api.RestfulResponse;
import com.koala.common.api.bean.RequestHeader;
import com.koala.order.dao.IOrderDao;
import com.koala.order.entity.OrderEntity;
import com.koala.order.rpc.repertory.request.SaveOutboundOrderRequest;
import com.koala.order.svc.IOrderSvc;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Date;

@Slf4j
@Service
public class OrderSvcImpl implements IOrderSvc {

    @Resource
    private IOrderDao orderDao;

    @Autowired
    private RestTemplate restTemplate;

    private static final String ORDER_SERVICE = "http://repertory-service/outbound/save";

    @GlobalTransactional(name = "order-service-create-order", rollbackFor = Exception.class)
    @Override
    public void save(OrderEntity orderEntity) {

        if (orderDao.xidExist(orderEntity.getXid()) > 0) {
            return;
        }

        orderDao.saveXID(orderEntity.getXid(), new Date());
        orderDao.save(orderEntity);
        invokRepertoryOutboundSaveService(orderEntity);

    }

    private void invokRepertoryOutboundSaveService(OrderEntity orderEntity) {

        RestfulRequest<SaveOutboundOrderRequest> request = new RestfulRequest();
        setRequestHeader(request);
        setSaveOutboundOrderRequest(request, orderEntity);

        RestfulResponse response = restTemplate.postForObject(ORDER_SERVICE,
                request,
                RestfulResponse.class);

        response.check();

    }

    private void setRequestHeader(RestfulRequest request) {
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setTimestamp(System.currentTimeMillis());
        request.setRequestHeader(requestHeader);
    }

    private void setSaveOutboundOrderRequest(RestfulRequest request, OrderEntity orderEntity) {
        request.setPayload(new SaveOutboundOrderRequest(orderEntity.getOrderNo()));
    }



    @Override
    public OrderEntity load(String id) {
        return orderDao.load(id);
    }
}
