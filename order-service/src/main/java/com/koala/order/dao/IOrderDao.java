package com.koala.order.dao;

import com.koala.order.entity.OrderEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Mapper
public interface IOrderDao {

    void save(OrderEntity order);

    void saveXID(@Param("xid") String xid, @Param("xidCreateTime") Date xidCreateTime);

    int xidExist(String xid);

    OrderEntity load(String id);

}
