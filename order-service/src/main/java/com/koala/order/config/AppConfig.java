package com.koala.order.config;

import com.alibaba.cloud.sentinel.annotation.SentinelRestTemplate;
import com.koala.common.sentinel.SentinelDefaultBlockHandler;
import com.koala.common.sentinel.SentinelDefaultFallback;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Reference
@Configuration
public class AppConfig {

    @Value("${application.start.model}")
    private String startModel;

    @Bean
    public void printStartModel() {
        System.out.println("................................." + startModel + ".................................");
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
