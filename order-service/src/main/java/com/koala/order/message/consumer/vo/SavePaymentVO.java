package com.koala.order.message.consumer.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SavePaymentVO {

    private String xid;
    private String id;
    private Double cost;
    private Date paymentCreateTime;

}
