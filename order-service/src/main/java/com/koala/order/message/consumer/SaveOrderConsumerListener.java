package com.koala.order.message.consumer;

import cn.hutool.core.lang.UUID;
import cn.hutool.json.JSONUtil;
import com.koala.order.entity.OrderEntity;
import com.koala.order.message.consumer.vo.SavePaymentVO;
import com.koala.order.svc.IOrderSvc;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import java.util.Date;

import static com.koala.order.message.consumer.ConsumerGroup.CONSUMER_GROUP;
import static com.koala.order.message.consumer.ConsumerTopic.SAVE_PAYMENT_MESSAGE_TOPIC;

@Component
@RocketMQMessageListener(consumerGroup = CONSUMER_GROUP, topic = SAVE_PAYMENT_MESSAGE_TOPIC)
public class SaveOrderConsumerListener implements RocketMQListener<String> {

    @Resource
    private IOrderSvc orderSvc;

    @Override
    public void onMessage(String message) {

        SavePaymentVO paymentVO = JSONUtil.toBean(message, SavePaymentVO.class);

        OrderEntity orderEntity = new OrderEntity();
        setOrderEntity(orderEntity, paymentVO);

        orderSvc.save(orderEntity);
    }

    private void setOrderEntity(OrderEntity order, SavePaymentVO paymentVO) {
        order.setId(UUID.randomUUID().toString());
        order.setOrderNo(UUID.randomUUID().toString());
        order.setOrderCreateTime(new Date());
        order.setCost(paymentVO.getCost());
        order.setXid(paymentVO.getXid());
    }
}
