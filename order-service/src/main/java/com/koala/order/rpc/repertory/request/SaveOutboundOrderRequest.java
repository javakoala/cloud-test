package com.koala.order.rpc.repertory.request;

import com.koala.common.api.base.BaseRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveOutboundOrderRequest extends BaseRequest {

    private String orderNo;

}
