package com.koala.order.api.request;

import com.koala.common.api.base.BaseRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderLoadRequest extends BaseRequest {

    private String id;

}
