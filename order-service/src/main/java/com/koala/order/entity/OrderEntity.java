package com.koala.order.entity;

import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderEntity {

    private String id;
    private String orderNo;
    private Double cost;
    private Date orderCreateTime;
    private String xid;

}
