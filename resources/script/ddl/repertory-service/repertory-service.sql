create schema if not exists `cloud-repertory` collate latin1_swedish_ci;

create table if not exists t_repertory
(
	id varchar(36) not null,
	outbound_order_no varchar(255) null,
	create_outbound_order_time datetime null,
	order_no varchar(255) null,
	constraint t_repertory_id_uindex
		unique (id)
);

alter table t_repertory
	add primary key (id);

create table if not exists undo_log
(
	id bigint auto_increment comment 'increment id'
		primary key,
	branch_id bigint not null comment 'branch transaction id',
	xid varchar(100) not null comment 'global transaction id',
	context varchar(128) not null comment 'undo_log context,such as serialization',
	rollback_info longblob not null comment 'rollback info',
	log_status int not null comment '0:normal status,1:defense status',
	log_created datetime not null comment 'create datetime',
	log_modified datetime not null comment 'modify datetime',
	constraint ux_undo_log
		unique (xid, branch_id)
)
comment 'AT transaction mode undo table' charset=utf8;