create schema if not exists `cloud-payment` collate latin1_swedish_ci;

create table if not exists t_payment
(
	id varchar(36) not null,
	cost double null,
	payment_create_time datetime null,
	xid varchar(36) null,
	constraint t_payment_id_uindex
		unique (id)
);

alter table t_payment
	add primary key (id);

create table if not exists t_payment_xid
(
	xid varchar(36) not null,
	xid_create_time datetime null,
	constraint t_payment_xid_xid_uindex
		unique (xid)
);

alter table t_payment_xid
	add primary key (xid);

create table if not exists undo_log
(
	id bigint auto_increment comment 'increment id'
		primary key,
	branch_id bigint not null comment 'branch transaction id',
	xid varchar(100) not null comment 'global transaction id',
	context varchar(128) not null comment 'undo_log context,such as serialization',
	rollback_info longblob not null comment 'rollback info',
	log_status int not null comment '0:normal status,1:defense status',
	log_created datetime not null comment 'create datetime',
	log_modified datetime not null comment 'modify datetime',
	constraint ux_undo_log
		unique (xid, branch_id)
)
comment 'AT transaction mode undo table' charset=utf8;