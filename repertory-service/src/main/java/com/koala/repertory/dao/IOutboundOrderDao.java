package com.koala.repertory.dao;

import com.koala.repertory.entity.OutboundOrderEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
public interface IOutboundOrderDao {

    void save(OutboundOrderEntity outboundOrder);


}
