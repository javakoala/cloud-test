package com.koala.repertory.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OutboundOrderEntity {

    private String id;
    private String outboundOrderNo;
    private Date createOutboundOrderTime;

    private String orderNo;

}
