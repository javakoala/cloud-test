package com.koala.repertory.ctrl;

import cn.hutool.core.util.IdUtil;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.koala.common.api.RestfulRequest;
import com.koala.common.api.RestfulResponse;
import com.koala.common.sentinel.SentinelDefaultBlockHandler;
import com.koala.common.sentinel.SentinelDefaultFallback;
import com.koala.repertory.api.request.SaveOutboundOrderRequest;
import com.koala.repertory.entity.OutboundOrderEntity;
import com.koala.repertory.svc.IOutboundOrderSvc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Slf4j
@RestController
@RequestMapping(value = "/outbound")
public class OutboundController {

    @Autowired
    private IOutboundOrderSvc outboundOrderSvc;


    /**
     * @SentinelResource中定义的value和@RequestMapping中定义的value不能相同
     * 否则熔断/降级回调会除问题
     * @param request
     * @return
     */
    @SentinelResource(value = "/resource/outbound/save",
            defaultFallback = "defaultFallback", fallbackClass = SentinelDefaultFallback.class,
            blockHandler = "defaultBlockHandler", blockHandlerClass = SentinelDefaultBlockHandler.class)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestfulResponse save(@RequestBody RestfulRequest<SaveOutboundOrderRequest> request) {

        OutboundOrderEntity order = new OutboundOrderEntity(
                IdUtil.randomUUID(),
                IdUtil.simpleUUID(),
                new Date(),
                request.getPayload().getOrderNo());

        outboundOrderSvc.save(order);

        return new RestfulResponse();
    }

}
