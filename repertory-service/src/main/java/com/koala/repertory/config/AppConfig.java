package com.koala.repertory.config;

import jdk.nashorn.internal.ir.annotations.Reference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Reference
@Configuration
public class AppConfig {

    @Value("${application.start.model}")
    private String startModel;

    @Bean
    public void printStartModel() {
        System.out.println("................................." + startModel + ".................................");
    }

}
