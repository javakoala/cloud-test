package com.koala.repertory.svc.impl;

import com.koala.repertory.dao.IOutboundOrderDao;
import com.koala.repertory.entity.OutboundOrderEntity;
import com.koala.repertory.svc.IOutboundOrderSvc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Slf4j
@Transactional(readOnly = true)
@Service
public class OutboundSvcImpl implements IOutboundOrderSvc {

    @Resource
    private IOutboundOrderDao outboundOrderDao;

    @Transactional
    @Override
    public void save(OutboundOrderEntity outboundOrderEntity) {
        outboundOrderDao.save(outboundOrderEntity);
        // Test global transaction rollback
        // int a = 1 / 0;
    }
}
