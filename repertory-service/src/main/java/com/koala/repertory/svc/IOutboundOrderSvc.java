package com.koala.repertory.svc;


import com.koala.repertory.entity.OutboundOrderEntity;

public interface IOutboundOrderSvc {

    void save(OutboundOrderEntity outboundOrderEntity);

}
