package com.koala.payment.dao;

import com.koala.payment.entity.PaymentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

@Mapper
public interface IPaymentDao {

    void save(PaymentEntity payment);

    void saveXID(@Param("xid") String xid, @Param("xidCreateTime") Date xidCreateTime);

    int xidExist(String xid);

    PaymentEntity load(String id);

}
