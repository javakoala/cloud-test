package com.koala.payment.api.request;

import com.koala.common.api.base.BaseRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SavePaymentRequest extends BaseRequest {

    private Double cost;

}
