package com.koala.payment.message.producer.tx;

import com.koala.payment.dao.IPaymentDao;
import com.koala.payment.entity.PaymentEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Slf4j
@Service
public class PaymentSaveTXMessageCallback {

    @Resource
    private IPaymentDao paymentDao;

    @Transactional
    public void savePaymentInTransaction(PaymentEntity payment) {
        if(paymentDao.xidExist(payment.getXid()) > 0) {
            return;
        }
        paymentDao.saveXID(payment.getXid(), new Date());
        paymentDao.save(payment);
    }

}
