package com.koala.payment.message.producer.tx;

import cn.hutool.json.JSONUtil;
import com.koala.payment.entity.PaymentEntity;
import com.koala.payment.message.producer.ProducerTransactionGroup;
import com.koala.payment.svc.IPaymentSvc;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


@Slf4j
@RocketMQTransactionListener(txProducerGroup = ProducerTransactionGroup.PAYMENT_SAVE_TX_PRODUCER_GROUP)
public class PaymentSaveProducerListener implements RocketMQLocalTransactionListener {

    @Resource
    private IPaymentSvc paymentSvc;

    @Resource
    private PaymentSaveTXMessageCallback paymentSaveTXMessageCallback;

    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message msg, Object arg) {

        PaymentEntity payment = getPaymentEntity(msg);

        try {
            paymentSaveTXMessageCallback.savePaymentInTransaction(payment);
        } catch (Exception e) {
            log.error(e.getMessage());
            return RocketMQLocalTransactionState.ROLLBACK;
        }

        return RocketMQLocalTransactionState.COMMIT;
    }

    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message msg) {

        PaymentEntity payment = getPaymentEntity(msg);

        if(paymentSvc.load(payment.getId()) == null) {
            return RocketMQLocalTransactionState.ROLLBACK;
        } else {
            return RocketMQLocalTransactionState.COMMIT;
        }

    }

    private PaymentEntity getPaymentEntity(Message msg) {

        String messageString = new String((byte[]) msg.getPayload());
        PaymentEntity payment = JSONUtil.toBean(messageString, PaymentEntity.class);

        return payment;
    }

}
