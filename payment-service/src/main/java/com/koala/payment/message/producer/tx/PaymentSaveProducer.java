package com.koala.payment.message.producer.tx;

import cn.hutool.json.JSONUtil;
import com.koala.payment.entity.PaymentEntity;
import com.koala.payment.message.producer.ProducerTransactionGroup;
import com.koala.payment.message.producer.ProducerTopic;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class PaymentSaveProducer {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    public void sendPaymentSaveMessage(PaymentEntity payment) {

        rocketMQTemplate.sendMessageInTransaction(
                ProducerTransactionGroup.PAYMENT_SAVE_TX_PRODUCER_GROUP,
                ProducerTopic.SAVE_PAYMENT_MESSAGE_TOPIC,
                getMqMessage(payment),
                null
        );

    }

    private Message getMqMessage(PaymentEntity payment) {
        String paymentJson = JSONUtil.parse(payment).toString();
        return MessageBuilder.withPayload(paymentJson).build();
    }

}
