package com.koala.payment.ctrl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.koala.common.api.RestfulRequest;
import com.koala.common.api.RestfulResponse;
import com.koala.common.sentinel.SentinelDefaultBlockHandler;
import com.koala.common.sentinel.SentinelDefaultFallback;
import com.koala.payment.api.request.SavePaymentRequest;
import com.koala.payment.svc.IPaymentSvc;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "/payment")
public class PaymentController {

    @Resource
    private IPaymentSvc paymentSvc;

    @SentinelResource(value = "/source/payment/save",
            defaultFallback = "defaultFallback", fallbackClass = SentinelDefaultFallback.class,
            blockHandler = "defaultBlockHandler", blockHandlerClass = SentinelDefaultBlockHandler.class)
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestfulResponse save(@RequestBody RestfulRequest<SavePaymentRequest> request) {
        paymentSvc.save(request.getPayload());
        return new RestfulResponse();
    }

}
