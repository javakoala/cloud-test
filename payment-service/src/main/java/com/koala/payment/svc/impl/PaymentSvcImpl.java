package com.koala.payment.svc.impl;

import cn.hutool.core.lang.UUID;
import com.koala.payment.api.request.SavePaymentRequest;
import com.koala.payment.dao.IPaymentDao;
import com.koala.payment.entity.PaymentEntity;
import com.koala.payment.message.producer.tx.PaymentSaveProducer;
import com.koala.payment.svc.IPaymentSvc;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class PaymentSvcImpl implements IPaymentSvc {

    @Resource
    private IPaymentDao paymentDao;

    @Resource
    private PaymentSaveProducer paymentSaveProducer;

    @Override
    public void save(SavePaymentRequest request) {

        PaymentEntity payment = new PaymentEntity();
        setPayment(payment, request);

        paymentSaveProducer.sendPaymentSaveMessage(payment);

    }

    @Transactional(readOnly = true)
    @Override
    public PaymentEntity load(String id) {
        return paymentDao.load(id);
    }

    private void setPayment(PaymentEntity payment, SavePaymentRequest request) {
        payment.setId(UUID.randomUUID().toString());
        payment.setXid(UUID.randomUUID().toString());
        payment.setCost(request.getCost());
        payment.setPaymentCreateTime(new Date());
    }

}
