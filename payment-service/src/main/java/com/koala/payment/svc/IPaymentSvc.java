package com.koala.payment.svc;

import com.koala.payment.api.request.SavePaymentRequest;
import com.koala.payment.entity.PaymentEntity;

public interface IPaymentSvc {

    void save(SavePaymentRequest request);

    PaymentEntity load(String id);
}
